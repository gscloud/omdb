<?php
/**
 * Find CSFD movies rating
 * php version 8.2
 *
 * @category Helper
 * @package  None
 * @author   Fred Brooker <git@gscloud.cz>
 * @license  MIT https://gscloud.cz/LICENSE
 * @link     None
 */

mb_internal_encoding('UTF-8');
require __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/config.php';

$cli = new \League\CLImate\CLImate;

// transliteration rules
$trans = [
    'á' => 'a',
    'à' => 'a',
    'á' => 'a',
    'ä' => 'a',
    'č' => 'c',
    'ć' => 'c',
    'č' => 'c',
    'ď' => 'd',
    'é' => 'e',
    'ě' => 'e',
    'è' => 'e',
    'é' => 'e',
    'ë' => 'e',
    'ě' => 'e',
    'í' => 'i',
    'í' => 'i',
    'ĺ' => 'l',
    'ľ' => 'l',
    'ḿ' => 'm',
    'ń' => 'n',
    'ň' => 'n',
    'ó' => 'o',
    'ö' => 'o',
    'ø' => 'o',
    'ř' => 'r',
    'ŕ' => 'r',
    'ř' => 'r',
    'ś' => 's',
    'š' => 's',
    'š' => 's',
    'ť' => 't',
    'ú' => 'u',
    'ú' => 'u',
    'ü' => 'u',
    'ů' => 'u',
    'ý' => 'y',
    'ý' => 'y',
    'ź' => 'z',
    'ž' => 'z',
    'ž' => 'z',
    'Á' => 'a',
    'À' => 'a',
    'Á' => 'a',
    'Ä' => 'a',
    'Č' => 'c',
    'Ć' => 'c',
    'Č' => 'c',
    'Ď' => 'd',
    'É' => 'e',
    'Ě' => 'e',
    'È' => 'e',
    'É' => 'e',
    'Ë' => 'e',
    'Ě' => 'e',
    'Í' => 'i',
    'Í' => 'i',
    'Ĺ' => 'l',
    'Ľ' => 'l',
    'Ḿ' => 'M',
    'Ň' => 'n',
    'Ń' => 'n',
    'Ó' => 'o',
    'Ö' => 'o',
    'Ø' => 'o',
    'Ř' => 'r',
    'Ŕ' => 'r',
    'Ř' => 'r',
    'Ś' => 's',
    'Š' => 's',
    'Š' => 's',
    'Ť' => 't',
    'Ú' => 'u',
    'Ú' => 'u',
    'Ü' => 'u',
    'Ů' => 'u',
    'Ý' => 'y',
    'Ý' => 'y',
    'Ź' => 'z',
    'Ž' => 'z',
    'Ž' => 'z',
];

$movies = @file(NAMES, FILE_IGNORE_NEW_LINES);
if (is_array($movies)) {
    $movies = \array_map(
        function ($value) {
            return trim($value);
        }, $movies
    );
} else {
    $cli->error("ERROR: Cannot read 'names' file!");
    exit(1);
}

$c = 0;
$t = count($movies);
// @phpstan-ignore-next-line
$progress = $cli->progress()->total($t);
$ch = curl_init();
foreach ($movies as $m) {
    $c++;
    $progress->current($c, "{$m}");
    usleep(100);
    if (file_exists(CSFD_JSON . "{$m}.json")) {
        continue;
    }
    curl_setopt($ch, CURLOPT_URL, CSFD_API_SEARCH . rawurlencode($m));
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $r = curl_exec($ch);
    if (is_string($r)) {
        $d = json_decode($r, true);
        @file_put_contents(CSFD_JSON . "{$m}.json", $r, LOCK_EX);
    } else {
        $cli->error("ERROR: Unable to fetch CSFD data for [$m].");
        continue;
    }
    if (is_array($d)) {
        if (isset($d['movies'][0])) {
            if (isset($d['movies'][0]['type'])) {
                $br = 0;
                switch ($d['movies'][0]['type']) {
                case 'film':
                case 'TV film':
                case 'amatérský film':
                case 'seriál':
                    break;
                default:
                    $br++;
                }
                if ($br) {
                    continue;
                }
            }
            $id = $d['movies'][0]['id'];
            $title = $d['movies'][0]['title'];
            $progress->current($c, "found: {$title}");
            usleep(100);
            $tr = str_replace(array_keys($trans), $trans, $title);
            $tr = trim(strtolower($tr));
            $pn = $m;
            $m = str_replace(array_keys($trans), $trans, $m);
            $m = trim(strtolower($m));
            if ($m === $tr) { // TBD: loose comparison
                $progress->current($c, "[$id] $title - $tr");
                usleep(100);
                curl_setopt($ch, CURLOPT_URL, CSFD_API_MOVIE . rawurlencode($id));
                $r = curl_exec($ch);
                if (is_string($r)) {
                    $d = json_decode($r, true);
                    @file_put_contents(CSFD_JSON . "detail-{$pn}.json", $r, LOCK_EX);
                } else {
                    $cli->error("ERROR: Unable to fetch CSFD data for [$m].");
                    continue;
                }
                if (is_array($d)) {
                    if (isset($d['id']) && isset($d['rating'])) {
                        $rating = $d['rating'];
                        $progress->current($c, "rating: {$rating}");
                        $out = "$tr;$title;$rating\n";
                        @file_put_contents(CSFD_OUT, $out, FILE_APPEND | LOCK_EX);
                        usleep(100);
                    }
                }
            }
        }
    }
    sleep(2);
}
$progress->current($c, " ");
