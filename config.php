<?php
/**
 * Constants for OMDB scripts
 * php version 8.2
 *
 * @category Helper
 * @package  None
 * @author   Fred Brooker <git@gscloud.cz>
 * @license  MIT https://gscloud.cz/LICENSE
 * @link     None
 */

define('CACHE', './cache.json');
define('CSFD_API_MOVIE', 'http://localhost:3000/movie/');
define('CSFD_API_SEARCH', 'http://localhost:3000/search/');
define('CSFD_JSON', './csfd_json/');
define('CSFD_OUT', './csfd.csv');
define('DB', './imdb.csv');
define('DB2', './csfd.csv');
define('EX1', './stats-NO-SCORES.txt');
define('EX2', './stats-WORST-30.txt');
define('EX3', './stats-BEST-30.txt');
define('EX4', './stats-ALL-by-SCORE.txt');
define('EX5', './stats-NO-SCORES-NAMES.txt');
define('EX6', './stats-BIGGEST-30.txt');
define('EX7', './stats-ALL-by-SIZE.txt');
define('FS', '/home/mxdpeep/DS1/FILM/');
define('NAMES', './stats-NO-SCORES-NAMES.txt');
define('OMDB', './omdb_json/');
define('SF', RecursiveIteratorIterator::SELF_FIRST);
define('SKDS', RecursiveDirectoryIterator::SKIP_DOTS);
