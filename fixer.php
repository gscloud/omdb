<?php
/**
 * Recursively fix folder names with ratings
 * php version 8.2
 *
 * @category Helper
 * @package  None
 * @author   Fred Brooker <git@gscloud.cz>
 * @license  MIT https://gscloud.cz/LICENSE
 * @link     None
 */

mb_internal_encoding('UTF-8');
require __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/config.php';

$cli = new \League\CLImate\CLImate;

// blocked filenames
$blocked = [
    'DOCUMENTARY',
    'FILM',
    'FILM_CZ',
    'TV',
    'TV_CZ',
    '123',
    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
    'G',
    'H',
    'CH',
    'I',
    'J',
    'K',
    'L',
    'M',
    'N',
    'O',
    'P',
    'Q',
    'R',
    'S',
    'T',
    'U',
    'V',
    'W',
    'X',
    'Y',
    'Z',
    'Č',
    'Ď',
    'Ň',
    'Ř',
    'Š',
    'Ť',
    'Ú',
    'Ž',
];

// transliteration rules
$trans = [
    'á' => 'a',
    'à' => 'a',
    'á' => 'a',
    'ä' => 'a',
    'č' => 'c',
    'ć' => 'c',
    'č' => 'c',
    'ď' => 'd',
    'é' => 'e',
    'ě' => 'e',
    'è' => 'e',
    'é' => 'e',
    'ë' => 'e',
    'ě' => 'e',
    'í' => 'i',
    'í' => 'i',
    'ĺ' => 'l',
    'ľ' => 'l',
    'ḿ' => 'm',
    'ń' => 'n',
    'ň' => 'n',
    'ó' => 'o',
    'ö' => 'o',
    'ø' => 'o',
    'ř' => 'r',
    'ŕ' => 'r',
    'ř' => 'r',
    'ś' => 's',
    'š' => 's',
    'š' => 's',
    'ť' => 't',
    'ú' => 'u',
    'ú' => 'u',
    'ü' => 'u',
    'ů' => 'u',
    'ý' => 'y',
    'ý' => 'y',
    'ź' => 'z',
    'ž' => 'z',
    'ž' => 'z',
    'Á' => 'a',
    'À' => 'a',
    'Á' => 'a',
    'Ä' => 'a',
    'Č' => 'c',
    'Ć' => 'c',
    'Č' => 'c',
    'Ď' => 'd',
    'É' => 'e',
    'Ě' => 'e',
    'È' => 'e',
    'É' => 'e',
    'Ë' => 'e',
    'Ě' => 'e',
    'Í' => 'i',
    'Í' => 'i',
    'Ĺ' => 'l',
    'Ľ' => 'l',
    'Ḿ' => 'M',
    'Ň' => 'n',
    'Ń' => 'n',
    'Ó' => 'o',
    'Ö' => 'o',
    'Ø' => 'o',
    'Ř' => 'r',
    'Ŕ' => 'r',
    'Ř' => 'r',
    'Ś' => 's',
    'Š' => 's',
    'Š' => 's',
    'Ť' => 't',
    'Ú' => 'u',
    'Ú' => 'u',
    'Ü' => 'u',
    'Ů' => 'u',
    'Ý' => 'y',
    'Ý' => 'y',
    'Ź' => 'z',
    'Ž' => 'z',
    'Ž' => 'z',
];

$cache = [];
$files = [];
$movies = [];
$renames = [];
$scores = [];

// read IMDB database
$d = 0;
if ($file = fopen(DB, 'r')) {
    while (($row = fgetcsv($file, 0, ';')) !== false) {
        $filename = $row[0];
        $name = $row[1];
        $score = $row[2];
        $tr = str_replace(array_keys($trans), $trans, $name);
        $tr = strtolower($tr);
        $hash = hash('sha1', $tr);
        if (isset($movies[$hash])) {
            $d++;
            $cli->shout(
                "$d. IMDB duplicate: {$name} - [{$files[$hash]}] vs [{$filename}]"
            );
        }            
        $files[$hash] = $filename;
        $movies[$hash] = $tr;
        $scores[$hash] = $score * 10; // float 0.1-10
    }
}
if ($d) {
    exit(1);
}
$cli->info('IMDB: ' . count($movies) . " items");

// read CSFD database
$d = 0;
if ($file = fopen(DB2, 'r')) {
    while (($row = fgetcsv($file, 0, ';')) !== false) {
        $filename = $row[0];
        $name = $row[1];
        $score = $row[2];
        $tr = str_replace(array_keys($trans), $trans, $name);
        $tr = strtolower($tr);
        $hash = hash('sha1', $tr);
        if (isset($movies[$hash])) {
            $d++;
            $cli->shout(
                "$d. ČSFD duplicate: {$name} - [{$files[$hash]}] vs [{$filename}]"
            );
        }            
        $files[$hash] = $filename;
        $movies[$hash] = $tr;
        $scores[$hash] = $score; // integer 1-100
    }
}

if ($d) {
    exit(1);
}

$cli->info('ČSFD: ' . count($movies) . " items");

$c = 0;
$t = 0;
$count = 0;
$cache = [];

// read FS from cache
if (file_exists(CACHE) && is_readable(CACHE)) {
    $cache = json_decode(@file_get_contents(CACHE) ?: '', true);
    if (is_array($cache)) {
        $t = count($cache);
    } else {
        $cache = [];
    }
}

// traverse filesystem
if ($t === 0 && file_exists(FS) && is_dir(FS)) {
    
    // @phpstan-ignore-next-line
    $progress = $cli->progress()->total(100);
    
    // recursive iterator
    $i = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(FS, SKDS), SF);
    $i->setMaxDepth(2);

    foreach ($i as $item) {
        $p = $i->getSubPath();
        $pn = $i->getFileName();
        if ($i->isDir() && strlen($pn) > 1) {
            if (is_numeric($pn)) {
                continue;
            }
            if (in_array($pn, $blocked)) {
                continue;
            }
            $c++;
            $t++;
            $progress->current($c, "{$t}. {$p} - {$pn}");
            if ($c > 99) {
                $c = 0;
            }
            $tr = str_replace(array_keys($trans), $trans, $pn);
            $tr = strtolower($tr);
            $h = hash('sha1', $tr);
            $cache[$h] = [
                "path" => $p,
                "name" => $pn,
            ];
            if (in_array($tr, $movies)) {                
                if (!isset($scores[$h])) {
                    continue;
                }
                $nn = "$pn [{$scores[$h]}%]";
                $renames[] = [
                    "from" => FS . "{$p}/{$pn}",
                    "to" => FS . "{$p}/{$nn}",
                ];
                $cache[$h] = [
                    "path" => $p,
                    "name" => $nn,
                ];
                $count++;
            }
        }
    }
    $progress->current(100, " ");
} else {

    // use cache
    $cli->info("Using FS cache.");
    $c = 0;
    // @phpstan-ignore-next-line
    $progress = $cli->progress()->total(count($cache));
    foreach ($cache as $h => $v) {
        $p = $v['path'];
        $pn = $v['name'];
        $tr = str_replace(array_keys($trans), $trans, $pn);
        $tr = strtolower($tr);
        $c++;
        $progress->current($c, "{$t}. {$p} - {$pn}");
        usleep(100);
        if (is_numeric($pn)) {
            continue;
        }
        if (in_array($tr, $movies)) {                
            if (!isset($scores[$h])) {
                continue;
            }
            $nn = "$pn [{$scores[$h]}%]";
            $renames[] = [
                "from" => FS . "{$p}/{$pn}",
                "to" => FS . "{$p}/{$nn}",
            ];
            $cache[$h] = [
                "path" => $p,
                "name" => $nn,
            ];
            $count++;
        }
    }
    $progress->current($c, " ");
}

$cli->info("Total items: {$t}\nRenames: {$count}");

$c = 0;
// @phpstan-ignore-next-line
$progress = $cli->progress()->total(count($renames));
// process renames
foreach ($renames as $r) {
    if (@rename($r['from'], $r['to'])) {
        $c++;
        $progress->current($c, $r['to']);
    } else {
        $cli->error("failed: {$r['from']} > {$r['to']}");
    }
}
if ($c) {
    $progress->current($c, "");
}

// copy to cloud
if (is_array($cache)) {
    if (file_put_contents(CACHE, json_encode($cache, JSON_PRETTY_PRINT), LOCK_EX)) {
        $cli->info("Cache saved.");
        copy(DB, FS . DB);
        copy(DB2, FS . DB2);
    }
}
