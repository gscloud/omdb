<?php
/**
 * Export various data from cache
 * php version 8.2
 *
 * @category Helper
 * @package  None
 * @author   Fred Brooker <git@gscloud.cz>
 * @license  MIT https://gscloud.cz/LICENSE
 * @link     None
 */

mb_internal_encoding('UTF-8');
require __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/config.php';

$cli = new \League\CLImate\CLImate;

// blocked filenames
$blocked = [
    'BBC',
    'extras',
    's01',
    's02',
    's03',
    's04',
    's05',
    's06',
    's07',
    's08',
    's09',
    's10',
    's11',
    's12',
    's13',
    's14',
    's15',
    's16',
    's17',
    's18',
    's19',
    's20',
    's21',
    's22',
    's23',
    's24',
    's25',
    's26',
    's27',
    's28',
    's29',
    's30',
    'season 01',
    'season 02',
    'season 03',
    'season 04',
    'season 05',
    'season 06',
    'season 07',
    'season 08',
    'season 09',
    'season 1',
    'season 10',
    'season 11',
    'season 12',
    'season 13',
    'season 14',
    'season 15',
    'season 16',
    'season 17',
    'season 18',
    'season 19',
    'season 2',
    'season 20',
    'season 21',
    'season 22',
    'season 23',
    'season 24',
    'season 25',
    'season 26',
    'season 27',
    'season 28',
    'season 29',
    'season 3',
    'season 4',
    'season 5',
    'season 6',
    'season 7',
    'season 8',
    'season 9',
    'season.1',
    'season.2',
    'season.3',
    'season.4',
    'season.5',
    'season.6',
    'season.7',
    'season.8',
    'season.9',
    'season.10',
    'season.11',
    'season.12',
    'season.13',
    'season.14',
    'season.15',
    'season.16',
    'season.17',
    'season.18',
    'season.19',
    'season.20',
    'season.21',
    'season.22',
    'season.23',
    'season.24',
    'season.25',
    'season.26',
    'season.27',
    'season.28',
    'season.29',
    'season.30',
    'season_1',
    'season_10',
    'season_11',
    'season_12',
    'season_13',
    'season_14',
    'season_2',
    'season_3',
    'season_4',
    'season_5',
    'season_6',
    'season_7',
    'season_8',
    'season_9',
    'season_x',
];

$s = $t = 0;
$cache = $sizes = $scores = [];

// read from cache
if (file_exists(CACHE) && is_readable(CACHE)) {
    $cache = json_decode(@file_get_contents(CACHE) ?: '', true);
    if (is_array($cache)) {
        if ($t = count($cache)) {
            @unlink(EX1);
            @unlink(EX5);

            // iterate
            foreach ($cache as $h => $v) {
                $name = $v['name'];
                $path = $v['path'];
                if (is_numeric($name)) {
                    continue;
                }
                if (in_array($name, $blocked)) {
                    continue;
                }
                $size = isset($v['size']) ? $v['size'] : null;
                if ($size) {
                    $sizes[$h] = $size;
                }

                // process rating
                $score = null;
                if (strpos($name, '%') !== false) {
                    preg_match('/ \[(\d+)%]/', $name, $score);
                    if (!isset($score[1])) {
                        continue;
                    }
                    $score = (int) $score[1];
                    if ($score !== null && $score > 0) {
                        $scores[$h] = $score;
                    }
                } else {
                    // missing ratings
                    file_put_contents(EX1, "$path/$name\n", FILE_APPEND | LOCK_EX);
                    file_put_contents(EX5, "$name\n", FILE_APPEND | LOCK_EX);
                    $s++;
                }
            }

            // 30 LOWEST RATINGS
            if (is_array($scores)) {
                $c = 0;
                asort($scores);
                @unlink(EX2);
                $cli->bold("\nLOWEST SCORE");
                foreach ($scores as $k => $v) {
                    $c++;
                    $n = "{$cache[$k]['path']}/{$cache[$k]['name']}";
                    if ($c <= 10) {
                        echo "{$c}. {$n}\n";
                    }
                    file_put_contents(EX2, "{$c}. {$n}\n", FILE_APPEND | LOCK_EX);
                    if ($c >= 30) {
                        break;
                    }
                }
            }

            // 30 HIGHEST RATINGS
            if (is_array($scores)) {
                $c = 0;
                arsort($scores);
                @unlink(EX3);
                $cli->bold("\nHIGHEST SCORE");
                foreach ($scores as $k => $v) {
                    $c++;
                    $n = "{$cache[$k]['path']}/{$cache[$k]['name']}";
                    if ($c <= 10) {
                        echo "{$c}. {$n}\n";
                    }
                    file_put_contents(EX3, "{$c}. {$n}\n", FILE_APPEND | LOCK_EX);
                    if ($c >= 30) {
                        break;
                    }
                }
            }

            // ALL BY RATING
            if (is_array($scores)) {
                $c = 0;
                arsort($scores);
                @unlink(EX4);
                foreach ($scores as $k => $v) {
                    $c++;
                    $n = "{$cache[$k]['path']}/{$cache[$k]['name']}";
                    $sz = '';
                    if (isset($cache[$k]['size'])) {
                        $sz = "{$cache[$k]['size']}GB";
                    }
                    file_put_contents(
                        EX4,
                        "{$c}. {$n} {$sz}\n",
                        FILE_APPEND | LOCK_EX
                    );
                }
            }

            // ALL BY SIZE
            if (is_array($sizes)) {
                $c = 0;
                arsort($sizes);
                @unlink(EX7);
                foreach ($sizes as $k => $v) {
                    $c++;
                    $n = "{$cache[$k]['path']}/{$cache[$k]['name']}";
                    file_put_contents(
                        EX7,
                        "{$c}. {$n} {$cache[$k]['size']}GB\n",
                        FILE_APPEND | LOCK_EX
                    );
                }
            }

            // BIGGEST FOLDERS
            if (is_array($sizes) && count($sizes)) {
                $c = 0;
                arsort($sizes);
                @unlink(EX6);
                $cli->bold("\nBIGGEST");
                foreach ($sizes as $k => $v) {
                    $c++;
                    $n = "{$cache[$k]['path']}/{$cache[$k]['name']}";
                    if ($c <= 10) {
                        echo "{$c}. {$n} {$cache[$k]['size']}GB\n";
                    }
                    file_put_contents(
                        EX6,
                        "{$c}. {$n} {$cache[$k]['size']}GB\n",
                        FILE_APPEND | LOCK_EX
                    );
                    if ($c >= 30) {
                        break;
                    }
                }
            }

            $cli->out("\n<green>Missing ratings: <bold>$s</bold> items</green>");
            exit(0);
        }
    }
}

$cli->error("ERROR: Cache is empty!");
exit(1);
