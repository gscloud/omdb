#!/bin/bash
#@author Fred Brooker <git@gscloud.cz>

source .env

mkdir -p ${OMDB_STORE}

# encoder
rawurlencode() {
  local string="${1}"
  local strlen=${#string}
  local encoded=""
  local pos c o
  for ((pos=0; pos<strlen; pos++)); do
     c=${string:$pos:1}
     case "$c" in
        [-_.~a-zA-Z0-9] ) o="${c}" ;;
        * ) printf -v o '%%%02x' "'$c"
     esac
     encoded+="${o}"
  done
  echo "${encoded}"
}

c=0
# the looper
while read line; do
    if [ -f "${OMDB_STORE}${line}.json" ]; then continue; fi

    ((c++))
    echo "${c}. ${line}"

    raw=$(rawurlencode "$line")
    result=$(curl -i -s \
            --connect-timeout 5 --max-time 5 \
            -H "Accept: application/json" \
            -H "Content-Type: application/json" \
            -X GET "https://www.omdbapi.com/?apikey=${OMDB_API_KEY}&t=${raw}" \
            | grep "Response" | jq '.')

    echo $result > "${OMDB_STORE}${line}.json"
    echo $result | jq '.imdbRating'

    if (( c == 500 )); then break; fi
    sleep 2

done < $INPUT_NO_SCORE_NAMES
