<?php
/**
 * Build IMDB database from JSON files
 * php version 8.2
 *
 * @category Helper
 * @package  None
 * @author   Fred Brooker <git@gscloud.cz>
 * @license  MIT https://gscloud.cz/LICENSE
 * @link     None
 */

mb_internal_encoding('UTF-8');
require __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/config.php';

$cli = new \League\CLImate\CLImate;

if (is_dir(OMDB)) {
    chdir(OMDB);
    $files = glob("*.json") ?: [];
    chdir('..');
} else {
    $cli->error("ERROR: directory '" . OMDB . "' does not exist!");
    exit(1);
}

unlink(DB);

$c = 0;
foreach ($files as $f) {
    if (is_file(OMDB . $f)) {
        $file = file_get_contents(OMDB . $f);
        if (is_string($file)) {
            $json = json_decode($file, true);
            if (is_array($json)) {
                if (isset($json['Title']) && isset($json['imdbRating'])) {
                    if (!is_numeric($json['imdbRating'])) {
                        continue;
                    }
                    $f = str_replace('.json', '', $f);
                    $f = strtr($f, ';', ',');
                    $json['Title'] = strtr($json['Title'], ';', ',');
                    $c++;
                    $csv = "$f;{$json['Title']};{$json['imdbRating']}\n";
                    file_put_contents(DB, $csv, FILE_APPEND | LOCK_EX);
                }
            }
        }
    }
}
$cli->output("OMDB database: <bold>$c</bold> lines");
