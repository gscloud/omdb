#@author Fred Brooker <git@gscloud.cz>
include .env

all:
	@echo "clearcache | fixer | exporter | sizer | db | fetch | stan | csfd | everything";

fixer:
	@php -f fixer.php

exporter:
	@php -f exporter.php
	@test -s stats-ALL-by-SCORE.txt && cat stats-ALL-by-SCORE.txt | grep DOCUMENTARY/ > stats-DOCUMENTARY-by-SCORE.txt
	@test -s stats-ALL-by-SCORE.txt && cat stats-ALL-by-SCORE.txt | grep FILM/ > stats-FILM-by-SCORE.txt
	@test -s stats-ALL-by-SCORE.txt && cat stats-ALL-by-SCORE.txt | grep FILM_CZ/ > stats-FILM_CZ-by-SCORE.txt
	@test -s stats-ALL-by-SCORE.txt && cat stats-ALL-by-SCORE.txt | grep TV/ > stats-TV-by-SCORE.txt
	@test -s stats-ALL-by-SCORE.txt && cat stats-ALL-by-SCORE.txt | grep TV_CZ/ > stats-TV_CZ-by-SCORE.txt
	@test -s stats-ALL-by-SIZE.txt && cat stats-ALL-by-SIZE.txt   | grep DOCUMENTARY/ > stats-DOCUMENTARY-by-SIZE.txt
	@test -s stats-ALL-by-SIZE.txt && cat stats-ALL-by-SIZE.txt   | grep FILM/ > stats-FILM-by-SIZE.txt
	@test -s stats-ALL-by-SIZE.txt && cat stats-ALL-by-SIZE.txt   | grep FILM_CZ/ > stats-FILM_CZ-by-SIZE.txt
	@test -s stats-ALL-by-SIZE.txt && cat stats-ALL-by-SIZE.txt   | grep TV/ > stats-TV-by-SIZE.txt
	@test -s stats-ALL-by-SIZE.txt && cat stats-ALL-by-SIZE.txt   | grep TV_CZ/ > stats-TV_CZ-by-SIZE.txt
	@cp stats-* /home/mxdpeep/DS1/FILM/
	@echo "export done"

sizer:
	@php -f sizer.php

clearcache:
	@rm -f cache.json
	@echo "cache cleared"

stan:
	@phpstan analyse -l 9 exporter.php fixer.php build_db.php csfd.php sizer.php

csfd:
	@-docker rm CSFD --force > /dev/null 2>&1
	@echo "Starting container"
	@docker run -d --name CSFD -p 3000:3000 bartholomej/node-csfd-api
	@sleep 3
	@php -f csfd.php
	@echo ""
	@docker rm CSFD --force > /dev/null 2>&1

db:
	@php -f ./build_db.php

fetch:
	@php -f exporter.php
	@bash ./fetch.sh

# macro
everything: fixer db fetch sizer exporter 
	@git add -A
	@git commit -am 'automatic update'
